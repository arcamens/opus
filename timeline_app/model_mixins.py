from django.core.urlresolvers import reverse
from wsbells.models import QueueWS

class TimelineMixin(QueueWS):
    """
    Mixins.
    """

    @classmethod
    def get_user_timelines(cls, user):
        timelines = user.timelines.filter(
            organization=user.default)
        return timelines

    def get_link_url(self):
        return reverse('timeline_app:timeline-link', 
                    kwargs={'timeline_id': self.id})

    def __str__(self):
        return self.name

class EUpdateTimelineMixin(object):
    pass

class EDeleteTimelineMixin(object):
    pass

class EUnbindTimelineUserMixin(object):
    pass

class ECreateTimelineMixin(object):
    pass

class EBindTimelineUserMixin(object):
    pass









