from post_app.models import EUnbindTagPost, ECreatePost, EUpdatePost, \
PostFileWrapper, EDeletePost, EAssignPost, EBindTagPost, EUnassignPost, \
PostFilter, GlobalPostFilter, ECutPost, EArchivePost, ECopyPost, GlobalTaskFilter, EUnarchivePost
from django.db.models import Q, F, Exists, OuterRef, Count, Sum
from core_app.models import Clipboard, Tag, User
from django.shortcuts import render, redirect
from core_app.views import GuardianView
from django.core.urlresolvers import reverse
from django.views.generic import View
from django.core.mail import send_mail
from django.http import HttpResponse
from jsim.jscroll import JScroll
from timeline_app.models import Timeline, EPastePost
from django.db.models.functions import Concat
from functools import reduce
from django.conf import settings
from . import forms
from . import models
from re import split
import operator
import json

class Post(GuardianView):
    """
    """

    def get(self, request, post_id):
        post = models.Post.objects.get(id=post_id)

        if not post.ancestor:
            return HttpResponse("This post is on clipboard!\
                It can't be accessed now.", status=403)

        user = User.objects.get(id=self.user_id)
        
        # Check if logged user has access to the post.
        has_access = user.timelines.filter(id=post.ancestor_id).exists()
        has_access = has_access or post.workers.filter(id=user.id).exists()

        if not has_access:
            return HttpResponse("You can't access this post!", status=403)

        return render(request, 'post_app/post.html', {'post':post,  })

class PostLink(GuardianView):
    """
    """

    def get(self, request, post_id):
        post = models.Post.objects.get(id=post_id)

        if not post.ancestor:
            return HttpResponse("This post is on clipboard!\
                It can't be accessed now.", status=403)

        user = User.objects.get(id=self.user_id)

        # Check if logged user has access to the post.
        has_access = user.timelines.filter(id=post.ancestor_id).exists()
        has_access = has_access or post.workers.filter(id=user.id).exists()

        if not has_access:
            return HttpResponse("You can't access this post!", status=403)

        attachments = post.postfilewrapper_set.all()
        workers = post.workers.all()

        organizations = user.organizations.exclude(id=user.default.id)

        queues = list(map(lambda ind: 'timeline%s' % ind, 
        user.timelines.values_list('id')))

        return render(request, 'post_app/post-link.html', 
        {'post':post, 'attachments': attachments, 
        'tags': post.tags.all(), 'workers': workers,
        'user': user, 'default': user.default, 'organization': user.default,
        'organizations': organizations, 'queues': json.dumps(queues),
         'settings': settings})

class CreatePost(GuardianView):
    """
    """

    def get(self, request, ancestor_id, post_id=None):
        ancestor   = Timeline.objects.get(id=ancestor_id)
        user       = User.objects.get(id=self.user_id)
        post       = models.Post.objects.create(user=user, ancestor=ancestor)
        form       = forms.PostForm(instance=post)
        post.label = 'Draft.'
        post.save()
        return render(request, 'post_app/create-post.html', 
        {'form':form, 'post': post, 'ancestor':ancestor})

    def post(self, request, ancestor_id, post_id):
        post     = models.Post.objects.get(id=post_id)
        ancestor = Timeline.objects.get(id=ancestor_id)

        form = forms.PostForm(request.POST, request.FILES, instance=post)
        if not form.is_valid():
            return render(request, 'post_app/create-post.html',
                        {'form': form, 'post':post, 
                                'ancestor': ancestor}, status=400)

        post.save()
        user  = User.objects.get(id=self.user_id)
        event = ECreatePost.objects.create(organization=user.default,
        timeline=ancestor, post=post, user=user)

        users = ancestor.users.all()
        event.dispatch(*users)

        user.ws_sound(post.ancestor)

        return redirect('timeline_app:list-posts', 
        timeline_id=ancestor_id)

class UpdatePost(GuardianView):
    def get(self, request, post_id):
        post = models.Post.objects.get(id=post_id)
        return render(request, 'post_app/update-post.html',
        {'post': post, 'form': forms.PostForm(instance=post)})

    def post(self, request, post_id):
        record  = models.Post.objects.get(id=post_id)
        form    = forms.PostForm(request.POST, request.FILES, instance=record)

        if not form.is_valid():
            return render(request, 'post_app/update-post.html',
                   {'post': record, 'form': form}, status=400)
        record.save()

        user  = User.objects.get(id=self.user_id)
        event = EUpdatePost.objects.create(organization=user.default,
        timeline=record.ancestor, post=record, user=user)

        event.dispatch(*record.ancestor.users.all())

        # Notify workers of the event, in case the post
        # is on a timeline whose worker is not on.
        event.dispatch(*record.workers.all())

        user.ws_sound(record.ancestor)

        return redirect('post_app:post', 
        post_id=record.id)


class AttachFile(GuardianView):
    """
    """

    def get(self, request, post_id):
        post = models.Post.objects.get(id=post_id)
        attachments = post.postfilewrapper_set.all()
        form = forms.PostFileWrapperForm()
        return render(request, 'post_app/attach-file.html', 
        {'post':post, 'form': form, 'attachments': attachments})

    def post(self, request, post_id):
        post = models.Post.objects.get(id=post_id)
        attachments = post.postfilewrapper_set.all()
        form = forms.PostFileWrapperForm(request.POST, request.FILES)

        if not form.is_valid():
            return render(request, 'post_app/attach-file.html', 
                {'post':post, 'form': form, 'attachments': attachments})
        record = form.save(commit = False)
        record.post = post
        form.save()
        return self.get(request, post_id)

class DetachFile(GuardianView):
    """
    """

    def get(self, request, filewrapper_id):
        filewrapper = PostFileWrapper.objects.get(id=filewrapper_id)
        filewrapper.delete()
        attachments = filewrapper.post.postfilewrapper_set.all()

        form = forms.PostFileWrapperForm()
        return render(request, 'post_app/attach-file.html', 
        {'post':filewrapper.post, 'form': form, 'attachments': attachments})

class DeletePost(GuardianView):
    def get(self, request, post_id):
        post = models.Post.objects.get(id = post_id)
        # post.delete()

        user  = User.objects.get(id=self.user_id)

        event = EDeletePost.objects.create(organization=user.default,
        timeline=post.ancestor, post_label=post.label, user=user)
        users = post.ancestor.users.all()
        event.dispatch(*users)

        ancestor = post.ancestor
        post.delete()

        user.ws_sound(post.ancestor)

        return redirect('timeline_app:list-posts', 
        timeline_id=ancestor.id)

class PostWorkerInformation(GuardianView):
    def get(self, request, peer_id, post_id):
        event = EAssignPost.objects.filter(post__id=post_id,
        peer__id=peer_id).last()

        active_tasks = event.peer.assignments.filter(done=False)
        done_tasks = event.peer.assignments.filter(done=True)
        active_tasks = active_tasks.count()
        done_tasks   = done_tasks.count()

        return render(request, 
        'post_app/post-worker-information.html',  
        {'peer': event.peer, 'active_tasks': active_tasks, 
         'post': event.post,  'done_tasks': done_tasks,
        'created': event.created, 'user':event.user})

class PostTagInformation(GuardianView):
    def get(self, request, tag_id, post_id):
        event = EBindTagPost.objects.filter(post__id=post_id,
        tag__id=tag_id).last()

        return render(request, 'post_app/post-tag-information.html', 
        {'user': event.user, 'created': event.created, 'tag':event.tag})

class UnassignPostUser(GuardianView):
    def get(self, request, post_id, user_id):
        user = User.objects.get(id=user_id)
        post = models.Post.objects.get(id=post_id)
        me   = User.objects.get(id=self.user_id)

        me.ws_sound(post.ancestor)

        event = EUnassignPost.objects.create(
        organization=me.default, ancestor=post.ancestor, 
        post=post, user=me, peer=user)

        event.dispatch(*post.ancestor.users.all())
        
        # As posts can be assigned to users off the timeline.
        # We make sure them get the evvent.
        event.dispatch(*post.workers.all())
        event.save()

        post.workers.remove(user)
        post.save()

        return HttpResponse(status=200)

class AssignPostUser(GuardianView):
    def get(self, request, post_id, user_id):
        user = User.objects.get(id=user_id)
        post = models.Post.objects.get(id=post_id)
        me = User.objects.get(id=self.user_id)

        post.workers.add(user)
        post.save()

        event = EAssignPost.objects.create(
        organization=me.default, ancestor=post.ancestor, 
        post=post, user=me, peer=user)

        event.dispatch(*post.ancestor.users.all())
        event.dispatch(*post.workers.all())
        event.save()

        me.ws_sound(post.ancestor)

        return HttpResponse(status=200)

class ManagePostWorkers(GuardianView):
    def get(self, request, post_id):
        me = User.objects.get(id=self.user_id)
        post = models.Post.objects.get(id=post_id)

        included = post.workers.all()
        excluded = me.default.users.exclude(assignments=post)
        total    = included.count() + excluded.count()

        return render(request, 'post_app/manage-post-workers.html', 
        {'included': included, 'excluded': excluded, 'post': post,
        'count': total, 'total': total, 'me': me, 
        'form':forms.UserSearchForm()})

    def post(self, request, post_id):
        sqlike = User.from_sqlike()
        form = forms.UserSearchForm(request.POST, sqlike=sqlike)

        me = User.objects.get(id=self.user_id)
        post = models.Post.objects.get(id=post_id)
        included = post.workers.all()
        excluded = me.default.users.exclude(assignments=post)
        total    = included.count() + excluded.count()

        if not form.is_valid():
            return render(request, 'post_app/manage-post-workers.html',  
                {'me': me, 'total': total, 'count': 0, 'post': post, 
                    'form':form}, status=400)

        included = sqlike.run(included)
        excluded = sqlike.run(excluded)
        count = included.count() + excluded.count()

        return render(request, 'post_app/manage-post-workers.html', 
        {'included': included, 'excluded': excluded, 'post': post,
        'me': me, 'form':form, 'total': total, 'count': count,})

class SetupPostFilter(GuardianView):
    def get(self, request, timeline_id):
        filter = PostFilter.objects.get(
        user__id=self.user_id, timeline__id=timeline_id)
        timeline = Timeline.objects.get(id=timeline_id)

        return render(request, 'post_app/setup-post-filter.html', 
        {'form': forms.PostFilterForm(instance=filter), 
        'timeline': timeline})

    def post(self, request, timeline_id):
        record = PostFilter.objects.get(
        timeline__id=timeline_id, user__id=self.user_id)
        sqlike = models.Post.from_sqlike()

        form     = forms.PostFilterForm(request.POST, sqlike=sqlike, instance=record)
        timeline = Timeline.objects.get(id=timeline_id)

        if not form.is_valid():
            return render(request, 'post_app/setup-post-filter.html',
                   {'timeline': record, 'form': form}, status=400)
        form.save()
        return redirect('timeline_app:list-posts', timeline_id=timeline.id)

class CutPost(GuardianView):
    def get(self, request, post_id):
        post          = models.Post.objects.get(id=post_id)
        user          = User.objects.get(id=self.user_id)
        timeline      = post.ancestor

        # Should have an event, missing creating event.
        user.ws_sound(post.ancestor)

        post.ancestor = None
        post.save()

        clipboard, _ = Clipboard.objects.get_or_create(
        user=user, organization=user.default)

        clipboard.posts.add(post)

        event = ECutPost.objects.create(organization=user.default,
        timeline=timeline, post=post, user=user)
        users = timeline.users.all()
        event.dispatch(*users)

        return redirect('timeline_app:list-posts', 
        timeline_id=timeline.id)

class CopyPost(GuardianView):
    def get(self, request, post_id):
        post = models.Post.objects.get(id=post_id)
        user = User.objects.get(id=self.user_id)
        copy = post.duplicate()

        clipboard, _    = Clipboard.objects.get_or_create(
        user=user, organization=user.default)
        clipboard.posts.add(copy)

        event = ECopyPost.objects.create(organization=user.default,
        timeline=post.ancestor, post=post, user=user)
        users = post.ancestor.users.all()
        event.dispatch(*users)

        user.ws_sound(post.ancestor)

        return redirect('timeline_app:list-posts', 
        timeline_id=post.ancestor.id)

class Done(GuardianView):
    def get(self, request, post_id):
        post      = models.Post.objects.get(id=post_id)
        post.done = True
        post.save()

        user = User.objects.get(id=self.user_id)

        # posts in the clipboard cant be archived.
        event = EArchivePost.objects.create(organization=user.default,
        timeline=post.ancestor, post=post, user=user)

        users = post.ancestor.users.all()
        event.dispatch(*users)

        user.ws_sound(post.ancestor)

        return redirect('post_app:post', 
        post_id=post.id)

class ManagePostTags(GuardianView):
    def get(self, request, post_id):
        me = User.objects.get(id=self.user_id)
        post = models.Post.objects.get(id=post_id)

        included = post.tags.all()
        excluded = me.default.tags.exclude(posts=post)

        return render(request, 'post_app/manage-post-tags.html', 
        {'included': included, 'excluded': excluded, 'post': post,
        'organization': me.default,'form':forms.TagSearchForm()})

    def post(self, request, post_id):
        sqlike = Tag.from_sqlike()

        form = forms.TagSearchForm(request.POST, sqlike=sqlike)

        me = User.objects.get(id=self.user_id)
        post = models.Post.objects.get(id=post_id)
        included = post.tags.all()
        excluded = me.default.tags.exclude(posts=post)

        if not form.is_valid():
            return render(request, 'post_app/manage-post-tags.html', 
                {'included': included, 'excluded': excluded,
                    'organization': me.default, 'post': post,
                        'form':form}, status=400)

        included = sqlike.run(included)
        excluded = sqlike.run(excluded)

        return render(request, 'post_app/manage-post-tags.html', 
        {'included': included, 'excluded': excluded, 'post': post,
        'me': me, 'organization': me.default,'form':form})

class UnbindPostTag(GuardianView):
    def get(self, request, post_id, tag_id):
        tag = Tag.objects.get(id=tag_id)
        post = models.Post.objects.get(id=post_id)
        post.tags.remove(tag)
        post.save()

        me = User.objects.get(id=self.user_id)

        event = EUnbindTagPost.objects.create(
        organization=me.default, ancestor=post.ancestor, 
        post=post, tag=tag, user=me)
        event.dispatch(*post.ancestor.users.all())
        event.save()

        me.ws_sound(post.ancestor)
        return HttpResponse(status=200)

class BindPostTag(GuardianView):
    def get(self, request, post_id, tag_id):
        tag = Tag.objects.get(id=tag_id)
        post = models.Post.objects.get(id=post_id)
        post.tags.add(tag)
        post.save()

        me = User.objects.get(id=self.user_id)

        event = EBindTagPost.objects.create(
        organization=me.default, ancestor=post.ancestor, 
        post=post, tag=tag, user=me)
        event.dispatch(*post.ancestor.users.all())
        event.save()

        me.ws_sound(post.ancestor)
        return HttpResponse(status=200)

class CancelPostCreation(GuardianView):
    def get(self, request, post_id):
        post = models.Post.objects.get(id = post_id)
        post.delete()

        return HttpResponse(status=200)

class Undo(GuardianView):
    def get(self, request, post_id):
        post      = models.Post.objects.get(id=post_id)
        post.done = False
        post.save()

        user = User.objects.get(id=self.user_id)
        event = EUnarchivePost.objects.create(organization=user.default,
        timeline=post.ancestor, post=post, user=user)

        users = post.ancestor.users.all()
        event.dispatch(*users)


        user.ws_sound(post.ancestor)

        return redirect('post_app:post', 
        post_id=post.id)


class RequestPostAttention(GuardianView):
    def get(self, request, peer_id, post_id):
        peer = User.objects.get(id=peer_id)
        post = models.Post.objects.get(id=post_id)

        form = forms.PostAttentionForm()
        return render(request, 'post_app/request-post-attention.html', 
        {'peer': peer,  'post': post, 'form': form})

    def post(self, request, peer_id, post_id):
        user = User.objects.get(id=self.user_id)
        peer = User.objects.get(id=peer_id)
        post = models.Post.objects.get(id=post_id)
        form = forms.PostAttentionForm(request.POST)

        if not form.is_valid():
            return render(request, 'post_app/request-post-attention.html', 
                    {'peer': peer, 'post': post, 'form': form})    

        url  = reverse('post_app:post-link', 
            kwargs={'post_id': post.id})

        url = '%s%s' % (settings.LOCAL_ADDR, url)
        msg = '%s (%s) has requested your attention on\n%s\n\n%s' % (
        user.name, user.email, url, form.cleaned_data['message'])

        send_mail('%s %s' % (user.default.name, 
        user.name), msg, user.email, [peer.email], fail_silently=False)

        return redirect('post_app:post-worker-information', 
        peer_id=peer.id, post_id=post.id)

class AlertPostWorkers(GuardianView):
    def get(self, request, post_id):
        post = models.Post.objects.get(id=post_id)
        user = User.objects.get(id=self.user_id)

        form = forms.AlertPostWorkersForm()
        return render(request, 'post_app/alert-post-workers.html', 
        {'post': post, 'form': form, 'user': user})

    def post(self, request, post_id):
        user = User.objects.get(id=self.user_id)
        post = models.Post.objects.get(id=post_id)
        form = forms.AlertPostWorkersForm(request.POST)

        if not form.is_valid():
            return render(request,'post_app/alert-post-workers.html', 
                    {'user': user, 'post': post, 'form': form})    

        url  = reverse('post_app:post-link', 
        kwargs={'post_id': post.id})

        url = '%s%s' % (settings.LOCAL_ADDR, url)
        msg = '%s (%s) has alerted you on\n%s\n\n%s' % (
        user.name, user.email, url, form.cleaned_data['message'])

        for ind in post.workers.values_list('email'):
            send_mail('%s %s' % (user.default.name, 
                user.name), msg, user.email, 
                    [ind[0]], fail_silently=False)

        return HttpResponse(status=200)

class ConfirmPostDeletion(GuardianView):
    def get(self, request, post_id):
        post = models.Post.objects.get(id=post_id)
        return render(request, 'post_app/confirm-post-deletion.html', 
        {'post': post})


class CreateFork(GuardianView):
    """
    """

    def get(self, request, ancestor_id, post_id, fork_id=None):
        post = models.Post.objects.get(id=post_id)
        user = User.objects.get(id=self.user_id)
        ancestor = Timeline.objects.get(id=ancestor_id)
        fork = models.Post.objects.create(user=user, 
        ancestor=ancestor, parent=post)

        form = forms.PostForm(instance=fork)
        fork.label = 'Draft.'

        path = post.path.all()
        fork.parent = post
        fork.path.add(*path, post)
        fork.save()

        return render(request, 'post_app/create-fork.html', 
        {'form':form, 'fork': fork, 'ancestor': ancestor, 'post': post})

    def post(self, request, ancestor_id, post_id, fork_id):
        post = models.Post.objects.get(id=post_id)
        fork = models.Post.objects.get(id=fork_id)
        form = forms.PostForm(request.POST, instance=fork)

        user = User.objects.get(id=self.user_id)

        if not form.is_valid():
            return render(request, 'post_app/create-fork.html', 
                {'form':form, 'ancestor': post.ancestor, 
                    'post': post, 'post': fork}, status=400)

        fork.save()

        event = models.ECreatePostFork.objects.create(organization=user.default,
        post_timeline=post.ancestor, fork_timeline=fork.ancestor, post=post, fork=fork, user=user)

        event.dispatch(*fork.ancestor.users.all())
        event.dispatch(*post.ancestor.users.all())

        user.ws_sound(post.ancestor)
        user.ws_sound(fork.ancestor)

        return redirect('timeline_app:list-posts', timeline_id=ancestor_id)

class SelectForkTimeline(GuardianView):
    def get(self, request, post_id):
        user = User.objects.get(id=self.user_id)
        post = models.Post.objects.get(id=post_id)
        form = forms.TimelineSearchform()
        timelines = Timeline.get_user_timelines(user)

        return render(request, 'post_app/select-fork-timeline.html', 
        {'form':form, 'post': post, 'elems': timelines})

    def post(self, request, post_id):
        form = forms.TimelineSearchform(request.POST)
        post = models.Post.objects.get(id=post_id)

        user  = User.objects.get(id=self.user_id)
        timelines = Timeline.get_user_timelines(user)

        if not form.is_valid():
            return render(request, 'post_app/select-fork-timeline.html', 
                  {'form':form, 'elems': timelines, 'post': post})

        timelines = timelines.annotate(text=Concat('name', 'description'))

        # Not sure if its the fastest way to do it.
        chks = split(' *\++ *', form.cleaned_data['pattern'])
        timelines = timelines.filter(reduce(operator.and_, 
        (Q(text__contains=ind) for ind in chks))) 

        return render(request, 'post_app/select-fork-timeline.html', 
        {'form':form, 'post': post, 'elems': timelines})

class UndoClipboard(GuardianView):
    def get(self, request, post_id):
        post = models.Post.objects.get(id=post_id)
        user = User.objects.get(id=self.user_id)
        event0 = post.e_copy_post1.last()
        event1 = post.e_cut_post1.last()

        # Then it is a copy because there is no event
        # mapped to it. A copy contains no e_copy_post1 nor
        # e_cut_post1.
        if not (event0 or event1):
            post.delete()
        else:
            self.undo_cut(event1)

        return redirect('core_app:list-clipboard')

    def undo_cut(self, event):
        user = User.objects.get(id=self.user_id)

        event.post.ancestor = event.timeline
        event.post.save()

        event1 = EPastePost(
        organization=user.default, timeline=event.timeline, user=user)
        event1.save(hcache=False)
        event1.posts.add(event.post)
        event.dispatch(*event.timeline.users.all())
        event1.save()
        
        clipboard, _ = Clipboard.objects.get_or_create(
        user=user, organization=user.default)

        clipboard.posts.remove(event.post)

class ListAllTasks(GuardianView):
    def get(self, request):
        me        = User.objects.get(id=self.user_id)
        filter, _ = GlobalTaskFilter.objects.get_or_create(
        user=me, organization=me.default)

        form  = forms.GlobalTaskFilterForm(instance=filter)

        posts = models.Post.get_allowed_posts(me)
        posts = posts.filter(Q(workers__isnull=False))
        total = posts.count()
        posts = filter.get_partial(posts)

        sqlike = models.Post.from_sqlike()
        sqlike.feed(filter.pattern)
        posts = sqlike.run(posts)

        count = posts.count()
        posts = posts.only('done', 'label', 'id').order_by('id')
        elems = JScroll(me.id, 'post_app/list-all-tasks-scroll.html', posts)

        return render(request, 'post_app/list-all-tasks.html', 
        {'total': total, 'count': count, 
        'form': form, 'elems': elems.as_div()})

    def post(self, request):
        me        = User.objects.get(id=self.user_id)
        filter, _ = GlobalTaskFilter.objects.get_or_create(
        user=me, organization=me.default)

        sqlike = models.Post.from_sqlike()
        form   = forms.GlobalTaskFilterForm(
            request.POST, sqlike=sqlike, instance=filter)

        posts = models.Post.get_allowed_posts(me)
        posts = posts.filter(Q(workers__isnull=False))
        total = posts.count()

        if not form.is_valid():
            return render(request, 'post_app/list-all-tasks.html', 
                {'form': form, 'total': total,
                    'count': 0}, status=400)

        form.save()

        posts = filter.get_partial(posts)
        posts = sqlike.run(posts)

        count = posts.count()
        posts = posts.only('done', 'label', 'id').order_by('id')
        elems = JScroll(me.id, 'post_app/list-all-tasks-scroll.html', posts)

        return render(request, 'post_app/list-all-tasks.html', 
        {'form': form, 'elems': elems.as_div(), 'total': total, 'count': count})

class Find(GuardianView):
    def get(self, request):
        me    = User.objects.get(id=self.user_id)

        filter, _ = GlobalPostFilter.objects.get_or_create(
        user=me, organization=me.default)
        form  = forms.GlobalPostFilterForm(instance=filter)

        posts = models.Post.get_allowed_posts(me)
        total = posts.count()

        sqlike = models.Post.from_sqlike()
        sqlike.feed(filter.pattern)

        posts = posts.filter(Q(done=filter.done))

        posts = sqlike.run(posts)
        count = posts.count()

        posts = posts.only('done', 'label', 'id').order_by('id')
        elems = JScroll(me.id, 'post_app/find-scroll.html', posts)

        return render(request, 'post_app/find.html', 
        {'form': form, 'elems':  elems.as_div(), 'total': total, 'count': count})

    def post(self, request):
        me        = User.objects.get(id=self.user_id)
        filter, _ = GlobalPostFilter.objects.get_or_create(
        user=me, organization=me.default)

        sqlike = models.Post.from_sqlike()
        form  = forms.GlobalPostFilterForm(request.POST, sqlike=sqlike, instance=filter)

        posts = models.Post.get_allowed_posts(me)
        total = posts.count()

        if not form.is_valid():
            return render(request, 'post_app/find.html', 
                {'form': form, 'total': total, 'count': 0}, status=400)
        form.save()

        posts  = posts.filter(Q(done=filter.done))
        posts  = sqlike.run(posts)
        count =  posts.count()

        posts = posts.only('done', 'label', 'id').order_by('id')
        elems = JScroll(me.id, 'post_app/find-scroll.html', posts)

        return render(request, 'post_app/find.html', 
        {'form': form, 'elems':  elems.as_div(), 'total': total, 'count': count})















