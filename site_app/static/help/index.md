## Intro

Splittask is a minimalist project management platform its focused on high productivity.
Splittask is built on three concepts: Organizations, Timelines and Posts, these entities
play a versatile role when modeling your business workflow. 

### Organization Creation

An organization can contain timelines, timelines can contain posts and posts hold comments.

Organizations can play different roles: Business Branch, Team, Project, Kanban Board ...
It all depends on your power of abstraction, necessity of controlling information and business's field.

After your signup you should have a default organization. For creating new organizations
just click on the Home link from the index page. You would be able to view a menu entry
whose name is your main organization. 

![organization-creation-0](/static/help/organization-creation-0.png)

You can then click on create it would lead you to:

![organization-creation-1](/static/help/organization-creation-1.png)

### Timeline Creation

You can create timelines for two kind of purposes: Task delegation, sharing information.
A timeline can be a big task itself.

After your organization creation it is time to create the timelines
over which tasks and information will be shared. 

For such, click on Home from the index page then:

![timeline-creation-0](/static/help/timeline-creation-0.png)

You would get:

![timeline-creation-1](/static/help/timeline-creation-1.png)

### Post Creation

A post can be a task or just a piece of information. You create a post, attach
the necessary content for the task execution then assign it to someone.

Imagine you have created a timeline named Todo. Access it by clicking
on the link of it. 

**Tip:** You can open it over another browser tab by
right clicking and opening in a new tab option. It is useful
sometimes to refer a timeline by its link. 

![post-creation-0](/static/help/post-creation-0.png)

After clicking on the timeline link you would get:

![post-creation-1](/static/help/post-creation-1.png)

From there you can access the most common timeline tools. 

Timeline posts contain a label that is a short description of the task/post.
It optionally can carry markdown content. You can attach multiple files to a given post.

![post-creation-2](/static/help/post-creation-2.png)

### Post Comments

Comments can be used for several kind of purposes. The comments in splittask
are viewed in a modal window thus allowing a separation between primary content
and secondary content.

Once a post is created you would be able to view it rendered on its timeline:

![post-comments-0](/static/help/post-comments-0.png)

Just click on the comment link you would get:

![post-comments-1](/static/help/post-comments-1.png)

### Tasks

A post plays a dual role in Splittask, it can be used for conveying information or
for conveying instructions for a task execution.

A post turns into a task when it is assigned to someone. It is possible to assign
a post to multiple workers.

In the post toolbar there is a link named Assign, click on it.

![tasks-0](/static/help/tasks-0.png)

Then you would be able to view:

![tasks-1](/static/help/tasks-1.png)

From there you can search through all your organization users by using
multiple attributes like user tag, e-mail or name.

You can assign a given post to someone who is not belonging to the post's timeline
as well. The worker gets notified after being assigned to the post and it will be
listed on the post Workers section. 

![tasks-2](/static/help/tasks-2.png)

It is possible to list all your tasks by clicking on the link Tasks from the index page.

![tasks-3](/static/help/tasks-3.png)

#### Task Notification

When a task is assigned to an worker, it gets notified through the platform but
sometimes it is useful to drop an e-mail to the worker, for such just press on the
worker link name:

![tasks-2](/static/help/tasks-2.png)

Click on request attention through e-mail:

![task-notification-0](/static/help/task-notification-0.png)

Then fill the form with an e-mail message:

![task-notification-1](/static/help/task-notification-1.png)

### Forks

Posts can be forked into sub posts, it is a powerful tool for maximizing efficiency
of a complex task execution. In splittask you can fork a given post over multiple
timelines. 

After clicking on:

![forks-0](/static/help/forks-0.png)

From the post toolbar, you would get a list of available timelines where the post
fork would stay on.

![forks-1](/static/help/forks-1.png)

Select the desired timeline then fill the post fields.
Once you have created the fork then the parent post will be available
on the fork.  

![forks-2](/static/help/forks-2.png)

If you click on the parent post link then you'll view the parent
post in detached view from its timeline.

![forks-3](/static/help/forks-3.png)

You can go through a given post subtasks easily using this approach.

### Markdown in Posts

Splittask uses github flavoured markdown, for more information: [Markdown Help](https://guides.github.com/features/mastering-markdown/) 

### Events

Splittask has a powerful event system, whatever it happens you'll be notified. Events
generall carry information that characterizes the event: User, Posts, Timelines etc.

![events-0](/static/help/events-0.png)

If you click on an event post/timeline link then it gets rendered and you can check what's going on.

**Tip:** You can open the event related posts/timelines in new browser tabs by right click the links then Open in New Tab. 

After checking all of a given event details you can just mark it as seen. It will be avaiable for later inspection
through the Logs entry at: Home -> Me -> Logs

![events-1](/static/help/events-1.png)

You'll be able to filter events based from a start and end date:

![events-2](/static/help/events-2.png)

### Timeline Members

Timeline members can be added/removed either by the timeline owner or existing members of the timeline.

For adding/removing users from a timeline click on the timeline link then : Settings.

![timeline-members-0](/static/help/timeline-members-0.png)

You would get:

![timeline-members-1](/static/help/timeline-members-1.png)

You can look up users from your current organization. The search can be based on tag, name or e-mail.

By tag:


    tag:developer + tag:python

By name:

    user_name


By e-mail:

    user@domain.com

After adding/removing an user he/she will get notified through an event.

### Post Filter

Posts contain information that is displayed on it is possible to filter a posts over a timeline
based on the following attributes: 

**Owner:** 
The user who has created the post.

**Worker:** 
The users who were assigned to the post.

**Created:**
When the post was created.

**Tag:** 
The tags the post contain.

**Comment:**
The comments it contains.

**Label:**
The post label content.

**Data:**
The post markdown content.

Splittask implements a simplistic and powerful filtering mechanism. The best way to explain
how it works it is through examples.

In a given timeline main toolbar click on Filter:

![post-filter-0](/static/help/post-filter-0.png)

**Note:** When the timeline post filter has a strike line on it means it is not on.
In such a situation then just active posts will be displayed. An active post is a
post that wasn't archived.

![post-filter-1](/static/help/post-filter-1.png)

Make sure you have Status checkbox marked otherwise it will remain deactivated.

#### Simplest Case

The simplest case it is when you're searching over the attributes: Label and Data.

![post-filter-2](/static/help/post-filter-2.png)

After pressing Setup you would end with merely:

![post-filter-3](/static/help/post-filter-3.png)

You could type as pattern several combinations:


    this is + creat
    
    this is a + crea
    
    th + is is + created
    
    created + this
    
    created + this is + a
    

All of the above examples would work as in the first pattern example.
It will display on the timeline all posts whose attributes label and data contains
the matching sequence of words

#### Filtering by Post Owner Attribute

Suppose you want to filter all posts that someone has created, what would you do?
Well, just set the following pattern:


    owner:name-of-the-person

Assuming the person is named oliver you could merely do:


    owner:oliver

and it would list all posts oliver has created.

You can as well filter by the post owner e-mail with:

    owner:person-e-mail

So you could do something like:

    owner:oliver@domain.com


The filter attributes are accumulative, so you could do as well:


    owner:fud + owner:main + owner:com

It would match all posts whose owner contains the string "fod" and "main" and "com"

#### Filtering by Tag Attribute

Tags can be added to posts thus allowing better classification of content.

Example:

    tag:bug + tag:django

The above example would filter all posts that contains the tags bug and django.


Example:
    
    rocket engine + tag:bug + project

The above example would match all posts that contains the string "rocket engine"
and the tag bug as well as the string "project" either in the post label or post markdown
content.

**Tip:** You can use the '+' sign to mix all kind of post attribute when filtering.

Example:

    tag:python + owner:iury

The above example would match all posts that is related to python and the creator is
a guy named iury.


Notice you could do as well:


    tag:python + owner:oliveira@domain.comm

Assuming the iury's guy e-mail was "oliveira@domain.com".


You can try eccentric patterns like:


    tag:pythn owner:oli + owner:.com

Which would match all posts that contain the tag python and the owner contains
the string "oli" and ".com" in its name or e-mail.

#### Filtering by other attributes

It is possible to mix up other attributes like:

    created:2018 + created:17 + tag:bug + owner:iury + rocket engine

You can combine with post comments too:


    owner:iury + comment:how to + comment:solve

Remember the attribute values are accumulative. In the above example it would find
all posts whose owner is iury and it contains one comment that contains the strings "how to" and "solve"
regardless of the order. Thus the above pattern is equivalent to:


    owner:iury + comment:solve + comment:how to 


### Timeline Filter

Timeline filters are a very handy feature. Timeline filters are mostly useful when
the timeline names/descriptions follow a defined format. 

Consider the situation that you have many projects and you're handling all them
in a single organization.

You could set your your project timeline's descriptions as: /Projects/Projectname


![timeline-filter-0](/static/help/timeline-filter-0.png)

Then whenever you want to view a given set of timelines that is related to a given project
you click on Home from the main view then Filter.

![timeline-filter-1](/static/help/timeline-filter-1.png)

You would get:

![timeline-filter-2](/static/help/timeline-filter-2.png)

Just insert your project path and click setup it will display just the timelines
whose description/name match your pattern. Make sure you have Status checkbox
marked otherwise the filter will not be actived.

### Task Searching

When someone is assigned to a post then it becomes a task. It is possible
to do task searching by clicking on: Tasks 


![task-searching-0](/static/help/task-searching-0.png)

After that you would get:

![task-searching-1](/static/help/task-searching-1.png)

If you mark as checked the item All Tasks then you'll be able to search
through all tasks from all timelines that you're a member.

If you select Created By Me then the search scope consists of all posts
that you have created and you belong to their timelines. 

When searching with Assigned to Me it allows you to find tasks that you're assigned regardless
if you belong to their timelines.

### Global Post Search

Splittask uses the same filter mechanism as in filtering timeline posts for searching for posts
globally, it means over all timelines that you're in.

Just click on: Home -> Find 

Then you'll get:

![global-post-search-0](/static/help/global-post-search-0.png)

Setup a pattern, if you want to search for archived posts only just leave the Done checkbox marked.

### Organization Invites

### Organization Admins

Organization admins are allowed to send invites and remove members. They aren't allowed to remove
other admins. Just the organization owner has the power for turning a regular user into an admin as well
as removing its status.

The organization owner can manage its admins by clicking on : Home -> Admins

![organization-admins-0](/static/help/organization-admins-0.png)

Then it would be shown to you a list of current admins and regular users:

![organization-admins-1](/static/help/organization-admins-1.png)

You can search organization users by using tags or the default attributes
like name and email.

You could do things like:


    tag:dev + tag:per + tag:py + .com

    
It would list prolly all python developers whose e-mail or name ends with the string '.com'.

### Organization User Removal

For removing a given user just click on: Home -> Members

![organization-user-removal-0](/static/help/organization-user-removal-0.png)

Then just select the user:

![organization-user-removal-1](/static/help/organization-user-removal-1.png)

You will be shown a list of timelines that you'll turn into the owner.

![organization-user-removal-2](/static/help/organization-user-removal-2.png)

### Timeline Deletion/Renaming

For deleting a timeline just get inside the timeline and press: Settings

![timeline-deletion-0](/static/help/timeline-deletion-0.png)

You would get:

![timeline-deletion-1](/static/help/timeline-deletion-1.png)

From there you can rename the timeline or just delete it. If you press
Delete button then you'll be prompted to confirm the timeline name.

### Organization Deletion/Renaming

When an organization is no longer necessary it is possible to delete it by clicking on: Home -> Settings

![organization-deletion-0](/static/help/organization-deletion-0.png)

You would get:

![organization-deletion-1](/static/help/organization-deletion-1.png)

From there you can either delete the organization or rename it.

After pressing Delete button you'll be asked to confirm the deletion by
inserting the organization name.








