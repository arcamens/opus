##############################################################################
# activate, virtualenv, opus.
cd ~/.virtualenvs/
source opus/bin/activate
cd ~/projects/opus-code

tee >(stdbuf -o 0 python -i)
tee >(python manage.py shell --settings=opus.settings)

quit()
##############################################################################

from django import forms
f = forms.CharField()
f.clean('foo')

f = forms.EmailField()
f.clean('foo@oo.com')
help(f.clean)

from django import forms
help(forms.Form.clean)
##############################################################################
from core_app.models import User
import datetime

me = User.objects.get(name__startswith='iury')
me.expiration = datetime.date.today() + datetime.timedelta(3)
(me.expiration - datetime.date.today()).days
##############################################################################
import os
os.rmdir('oaisduoi')
help(os.rmdir)

import shutil
shutil.rmtree('oiausoidu', ignore_errors=True)
shutil.rmtree('oiausoidu')
##############################################################################

from django.apps import apps
apps.get_app_config('admin').verbose_name
mod = apps.get_app_config('admin')
mod.module
mod.path

globals()[mod]
getattr(, function_name)

##############################################################################

from django.core.paginator import Paginator, EmptyPage
help(Paginator)


from django.template.loader import get_template
from django.template import Context

##############################################################################

from core_app.models import *
from card_app.models import *
from board_app.models import *
from list_app.models import *
from post_app.models import *
from timeline_app.models import *
from site_app.models import *
from note_app.models import *
from snippet_app.models import *
from comment_app.models import *

events = Event.objects.all()

def normalize_events(events):
    for ind in events:
        ind.create_html_cache()

events = ECreateCard.objects.all()
normalize_events(events)
events = EUpdateCard.objects.all()
normalize_events(events)
events = EUpdateNote.objects.all()
normalize_events(events)
##############################################################################
from post_app.models import Post
queryset = Post.objects.filter(label__startswith='f')
print(queryset)
print(queryset.query)
qs = str(queryset.query)
type(queryset.query)
from django.db.models import QuerySet
from django.db.models.sql.query import Query
old = QuerySet(query=Query(qs))
print(old)
dir(Query)
help(Query)
from django.db.models import Manager
Manager.raw(qs)
Post.objects.raw(qs)

import pickle
pickle.dumps(queryset.query)
l0 = pickle.dumps(queryset)
l1 = pickle.loads(l0)

print(l1)


import pickle
l0 = pickle.dumps(queryset)
l1 = pickle.loads(l0)
print(l1[0].__str__)

from django.core.cache import cache
len(cache)
dir(cache)
cache.get('jscroll-jsim/find-scroll.html')
cache.set('foo', 2, 2)
cache.get('foo')

help(cache.set)
from timeline_app.models import Timeline
from core_app.models import Organization, User
from post_app.models import Post
user = User.objects.get(name__startswith='iury')

organization1 = Organization.objects.get(name__startswith='Splittask' )

timeline = Timeline.objects.create(name='cool', organization=organization1, owner=user)
timeline.users.add(user)

for ind in range(100):
    Post.objects.create(label='foo%s' % ind, ancestor=timeline)


##############################################################################
# access victor vps and find mysql passsword for opus.
tee >(stdbuf -o 0 ssh opus@staging.arcamens.com 'bash -i')

cd ~/.virtualenv/
source opus/bin/activate
cd ~/projects/opus
tee >(python manage.py shell --settings=opus.settings)

from django.db import connection
db_name = connection.settings_dict['NAME']
db_name
connection.settings_dict
##############################################################################
from site_app.forms import ServiceForm
x = ServiceFor
help(ServiceForm)
##############################################################################
from datetime import datetime, date
import datetime
dir(datetime)
x = datetime(day=2, month=2, year=2018)
y = datetime(day=2, month=2, year=2012)
z = x - y
z.days
x = date(day=2, month=2, year=2018)
y = date(day=2, month=2, year=2012)
z = x - y
z
z.days

from django import forms

expiration = forms.DateField(widget=forms.SelectDateWidget())

help(forms.SelectDateWidget)

