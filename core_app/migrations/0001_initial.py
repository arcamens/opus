# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-10 11:36
from __future__ import unicode_literals

import core_app.models
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Clipboard',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now=True, null=True)),
                ('html', models.TextField(null=True)),
            ],
            bases=(core_app.models.EventMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Invite',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('token', models.CharField(max_length=256, null=True)),
                ('invite_url', models.CharField(max_length=256, null=True)),
                ('created', models.DateTimeField(auto_now=True, null=True)),
            ],
            bases=(core_app.models.InviteMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256, null=True, verbose_name='Name')),
                ('expiration', models.DateTimeField(blank=True, null=True)),
                ('created', models.DateTimeField(auto_now=True, null=True)),
            ],
            bases=(core_app.models.OrganizationMixin, models.Model),
        ),
    ]
