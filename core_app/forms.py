from django.utils.translation import ugettext_lazy as _
from timeline_app.forms import ConfirmTimelineDeletionForm
from slock.forms import SetPasswordForm
from sqlike.forms import SqLikeForm
from django import forms
from . import models
import datetime


class OrganizationForm(forms.Form):
    name = forms.CharField()

class EventSearchForm(forms.Form):
    pattern = forms.CharField()
    seen = forms.BooleanField(required=False)

class EventFilterForm(forms.Form):
    val0  = datetime.date.today()-datetime.timedelta(days=3)
    start = forms.DateField(initial = val0, widget=forms.SelectDateWidget())

    val1 = datetime.date.today()+datetime.timedelta(days=1)
    end  = forms.DateField(initial = val1, widget=forms.SelectDateWidget())

class TagSearchForm(SqLikeForm, forms.Form):
    pattern = forms.CharField(required=False, 
    help_text='Example:deve + heroes')

class OrganizationInviteForm(forms.Form):
    email = forms.EmailField(help_text="Insert user E-mail.")

class UserForm(forms.ModelForm):
    class Meta:
        model  = models.User
        fields = ('name', 'avatar', 'email')

class UserFilterForm(SqLikeForm, forms.ModelForm):
    class Meta:
        model  = models.UserFilter
        exclude = ('user', 'organization')

class TagForm(forms.ModelForm):
    class Meta:
        model  = models.Tag
        exclude = ('organization', )

class UpdateOrganizationForm(forms.ModelForm):
    class Meta:
        model = models.Organization
        fields = ( 'name', )

class ConfirmOrganizationDeletionForm(ConfirmTimelineDeletionForm):
    name = forms.CharField(required=True,
    help_text='Type the organization name to confirm!')

class RemoveUserForm(forms.Form):
    reason = forms.CharField(required=False, 
    help_text='Thank you for your participation!')

class UserSearchForm(SqLikeForm, forms.Form):
    pattern = forms.CharField(required=False,
    help_text='tag:developer + tag:python')

class SignupForm(SetPasswordForm):
    class Meta:
        model   = models.User
        exclude = ('organizations', 'default', 'service', 
        'expiration', 'max_users', 'paid')


